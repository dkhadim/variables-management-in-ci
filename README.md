# Variables Management In Ci

## Description

Ce repository permet d'illustrer l'utilisation des variables dans les différents jobs et de manière globale.

## Exemple

```yaml

variables:
    MY_VARIABLE: "default_value"

stages:
    - job1
    - job2

job1:
    stage: job1
    variables:
        MY_VARIABLE: "1"
    script: echo $MY_VARIABLE

job2:
    stage: job2
    variables:
        MY_VARIABLE: "2"
    script: echo $MY_VARIABLE

```
